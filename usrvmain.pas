unit usrvmain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  OverbyteIcsSysLogServer;

type
  Tkraeglogcatcher = class(TService)
    srvSysLog: TSysLogServer;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure srvSysLogDataAvailable(Sender: TObject; const SrcIP, SrcPort,
      RawMessage: AnsiString);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceShutdown(Sender: TService);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  kraeglogcatcher: Tkraeglogcatcher;

implementation
uses
  uconst;
{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  kraeglogcatcher.Controller(CtrlCode);
end;

function Tkraeglogcatcher.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure Tkraeglogcatcher.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  AppendDataToLog(gLogFileName, 'Service Continue');
  srvSysLog.Listen;
end;

procedure Tkraeglogcatcher.ServiceExecute(Sender: TService);
const
  SecBetweenRuns = 10;
var
  Count: Integer;
begin
  Count := 0;
  while not Terminated do begin
    Inc(Count);
    if Count >= SecBetweenRuns then begin
      Count := 0;
      //
    end;
    Sleep(1000);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure Tkraeglogcatcher.ServicePause(Sender: TService; var Paused: Boolean);
begin
  AppendDataToLog(gLogFileName, 'Service Pause');
  srvSysLog.Close;
end;

procedure Tkraeglogcatcher.ServiceShutdown(Sender: TService);
begin
  AppendDataToLog(gLogFileName, 'Service Shutdown');
  srvSysLog.Close;
end;

procedure Tkraeglogcatcher.ServiceStart(Sender: TService; var Started: Boolean);
begin
  gLogFileName := GetCurrentFileLog(ParamStr(0));
  AppendDataToLog(gLogFileName, 'Service Start');
  srvSysLog.Listen;
  Started := True;
end;

procedure Tkraeglogcatcher.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  AppendDataToLog(gLogFileName, 'Service Stop');
  srvSysLog.Close;
  Stopped := True;
end;

procedure Tkraeglogcatcher.srvSysLogDataAvailable(Sender: TObject; const SrcIP,
  SrcPort, RawMessage: AnsiString);
begin
  AppendDataToLog(gLogFileName, RawMessage);
end;

end.
