unit uconst;

interface

const
  cAppName = 'KraEg WebLog Catcher';
  cAppVers = '0.1';
  cFullName = cAppName + ' v.' + cAppVers;

function GetCurrentFileLog(aExeName: WideString) : WideString;
procedure AppendDataToLog(const cFileName: WideString; const cData : WideString);

var
  gLogFileName : WideString;

implementation
uses
  System.SysUtils;

function GetCurrentFileLog(aExeName: WideString) : WideString;
var
  fDate : TDateTime;
  fFileName : String;
begin
  fDate := System.SysUtils.Date;
  System.SysUtils.DateTimeToString(fFileName, 'yyyy-mm-dd', fDate);
  Result := System.SysUtils.ExtractFilePath(aExeName) + fFileName + '.log';
end;

procedure AppendDataToLog(const cFileName: WideString; const cData : WideString);
var f : TextFile;
    s : String;
    r : WideString;
begin
  AssignFile(f, cFileName);
  try
    if System.SysUtils.FileExists(cFileName) then Append(f)
    else Rewrite(f);
    System.SysUtils.DateTimeToString(s, 'yyyy-mm-dd', System.SysUtils.Date);
    r := '' + s + Chr(27) + '' + cData;
    WriteLn(f, r);
    CloseFile(f);
  except on E: Exception do
  end;
end;

end.
