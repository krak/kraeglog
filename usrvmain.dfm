object kraeglogcatcher: Tkraeglogcatcher
  OldCreateOrder = False
  DisplayName = 'KraEg Log Catcher'
  StartType = stBoot
  OnContinue = ServiceContinue
  OnExecute = ServiceExecute
  OnPause = ServicePause
  OnShutdown = ServiceShutdown
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 150
  Width = 215
  object srvSysLog: TSysLogServer
    Addr = '0.0.0.0'
    Port = '514'
    RelaxedSyntax = True
    OnDataAvailable = srvSysLogDataAvailable
    Left = 128
    Top = 56
  end
end
